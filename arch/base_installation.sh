#!/bin/sh

# VARIABLES
#
# 	Networking
local_ip_mask_prefix="24"
local_ip_address="192.168.42.43"
local_ip_gateway="192.168.42.1"
eth_name="enp13s0"
bridge_name="br0"
# 	Git
git_user_name="Skaii Nyght"
git_user_email="SkaiiNyght@protonmail.com"
#	User
user_name="skaiinyght"
#	Directories
user_directory=/home/$user_name
documents_directory=$user_directory/documents
dot_files_directory=$documents_directory/dot_files


# enable parallel downloads
sudo sed -i 's/\#ParallelDownloads/ParallelDownloads/g' /etc/pacman.conf


# Set up initial networking
function set_up_networking {
	sudo ip link add $bridge_name type bridge
	sudo ip link set $eth_name master $bridge_name
	sudo ip link set $eth_name up
	sudo ip link set $bridge_name up
	sudo ip address add $local_ip_address/$local_ip_mask_prefix dev $bridge_name
	sudo ip route add default via $local_ip_gateway dev $bridge_name
	sudo sh -c 'echo "nameserver 192.168.69.200" > /etc/resolv.conf'
	sudo sh -c 'echo "search skaiinyght.com" sudo >> /etc/resolv.conf'
}
if [ "'ping -c 1 google.com'" ]
then
		echo 'Ping not available from google, setting up network'
		set_up_networking
else
		echo 'Ping returned from google, skipping network setup'
fi
sleep 2 

# Install packages
sudo pacman --noconfirm -Syyu \
		vim \
		man-pages \
		man-db \
		texinfo \
		bspwm \
		sxhkd \
		polybar \
		feh \
		xorg-server \
		xorg-xrandr \
		xorg-xinit \
		rofi \
		xbindkeys \
		zsh \
		zsh-autosuggestions \
		zsh-completions \
		zsh-syntax-highlighting \
		zsh-theme-powerlevel10k \
		nvidia-open-dkms \
		nvidia-utils \
		nvidia-settings \
		lxappearance \
		kdeconnect \
		breeze-gtk \
		wget \
		stow \
		pacman-contrib \
		git \
		seahorse \
		openssh \
		gnome-keyring \
		lxsession \
		xdg-user-dirs \
		alacritty \
		nextcloud-client \
		discord \
		carla \
		pipewire \
		qpwgraph \
		pipewire-alsa \
		pipewire-pulse \
		pipewire-jack \
		wireplumber \
		unzip \
		btop \
		ranger \
	    picom \
		vivaldi \
		vivaldi-ffmpeg-codecs \
		neofetch \
		net-tools \
		pipewire \
		wireplumber \
		qpwgraph \
		pipewire-alsa \
		pipewire-jack \
		pamixer \
		jq \
		pavucontrol \
		python-virtualenv \
		python-wheel \
		base-devel \
		openssl \
		zlib \
		git \
		gobject-introspection \
		gtk3 \
		python-gobject \
		qemu-desktop \
		libvirt \
		edk2-ovmf \
		virt-manager \
		swtpm \
		virt-viewer \
		cups \
		nss-mdns \
		avahi \
		cronie \


# Configure Git
git config --global user.name "$git_user_name"
git config --global user.email "$git_user_email"

# Create xdg user directories
mkdir /home/$user_name/{desktop,downloads/tools,templates,public,documents,music,pictures,videos} -p

##
####
###### Setting up the tools folder
####
##


if [ -d $user_directory/downloads/tools ]
then
else
		mkdir $user_directory/downloads/tools
fi

# Copy down git repo
cd $documents_directory

git clone https://gitlab.com/SkaiiNyght/dot_files

# Stow the arch configs
cd $dot_files_directory

stow -t $user_directory -d $dot_files_directory -S arch
xdg-user-dirs-update

##
####
###### Services
####
##

function enable_virtualization_service {
	sudo systemctl enable --now libvirtd
}
function enable_cronie_service {
	sudo systemctl enable --now cronie
}
function enable_avahi_daemon {
		sudo systemctl enable --now avahi-daemon
		sudo sed -i 's/hosts: mymachines resolve/hosts: mymachines mdns_minimal [NOTFOUND=return]/g' /etc/nsswitch.conf
		sudo systemctl enable --now cups
}
function copy_service_files {
	# Copy the /usr/local/bin files
	sudo cp $dot_files_directory/root/arch/usr/local/bin/* -R /usr/local/bin

	# Copy the service files
	sudo cp $dot_files_directory/root/arch/etc/systemd/system/* /etc/systemd/system/

	# Enable copied services
	sudo systemctl daemon-reload
	sudo systemctl enable --now backup_fantasy_grounds.timer
	sudo systemctl enable --now mirror_refresh.timer
	sudo systemctl enable --now network_initialization.service
}
if [ $(sudo systemctl status cronie | grep disabled | wc -l) -gt 0]
then
	enable_cronie_service
else
		echo 'cronie service already enabled'
fi
if [ $(sudo systemctl status avahi-daemon | grep disabled | wc -l) -gt 0 ]
then
	enable_avahi_daemon
else
		echo 'avahi daemon already enabled'
fi

if [-e /usr/local/bin/mirror_refresh.sh]
then
		echo '/usr/local/bin files have already been moved, skipping copy'
else
		copy_service_files
fi
if [ $(sudo systemctl status libvirtd | grep disabled | wc -l) -gt 0 ]
then
	enable_virtualization_service
else
	echo 'libvirtd already enabled'
fi

function add_gnome_keyring_to_pam {
# Set up gnome-keyring
sudo sh -c "echo 'auth		optional		pam_gnome_keyring.so' >> /etc/pam.d/login"
sudo sh -c "echo 'session	optional		pam_gnome_keyring.so auto_start' >> /etc/pam.d/login"
}
if grep -q pam_gnome_keyring.so /etc/pam.d/login; then
		echo 'gnome keyring has already been added to the pam.d login file, skipping step'
else
	add_gnome_keyring_to_pam {
}


# Set up wallpaper folder
cd $user_directory/pictures

git clone https://gitlab.com/skaiinyght/wallpaper




##
####
###### Font Installation
####
##

function install_fira_code_fonts {
	sudo mkdir /usr/share/fonts/Fira_Code
	cd $user_directory/downloads/tools
	wget https://github.com/ryanoasis/nerd-fonts/releases/download/v2.1.0/FiraCode.zip
	sudo unzip ./FiraCode.zip -d /usr/share/fonts/Fira_Code
}
# Check if there are any existing fonts with 'fira' in the name, if so skip
if [ $(fc-list | grep -ci fira) -gt 0 ]
then
		echo 'Fira_Code fonts already installed skipping step'
else
		echo 'Installing fira code fonts'
		install_fira_code_fonts
fi

function install_weather_icon_fonts {
	sudo mkdir /usr/share/fonts/Weather_Icons
	cd $user_directory/downloads/tools
	wget https://github.com/erikflowers/weather-icons/archive/master.zip
	sudo unzip weather-icons-master.zip -d /usr/share/fonts/Weather_Icons
}
# Check if there are any existing fonts with 'weather' int he name, if so skip
if [ $(fc-list | grep -ci weather) -gt 0 ]
then
		echo 'Weather icons already installed skipping step'
else
		echo 'Installing Weather Icons'
fi

echo 'Done'
